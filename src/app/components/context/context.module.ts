import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContextComponent } from './context.component';
import { MaterialModule } from 'src/app/modules/material/material.module';

@NgModule({
  declarations: [ContextComponent],
  imports: [CommonModule, MaterialModule],
  exports: [ContextComponent],
})
export class ContextModule {}

import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent {
  @Output() submitComment: EventEmitter<string> = new EventEmitter();
  text = '';

  addComment(): void{
    this.submitComment.next(this.text);
  }
}

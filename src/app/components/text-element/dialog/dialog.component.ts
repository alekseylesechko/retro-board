import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BoardService } from 'src/app/containers/board/board.service';
import { Column } from 'src/app/containers/board/column/column.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  name = '';
  public color = 'green';
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    public boardService: BoardService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAddColumn(): void {
    if (this.name.trim()) {
      const column = new Column(this.name, this.color);
      this.boardService.addColumn(column);
      this.dialogRef.close();
    }
  }
}

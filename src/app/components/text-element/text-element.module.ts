import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { CommentComponent } from './comment/comment.component';
import { DialogComponent } from './dialog/dialog.component';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  declarations: [CommentComponent, DialogComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ColorPickerModule],
  exports: [CommentComponent, DialogComponent],
})
export class TextElementModule {}

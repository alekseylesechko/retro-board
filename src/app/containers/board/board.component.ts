import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Column } from './column/column.model';
import { BoardService } from './board.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit, OnDestroy {
  columns!: Column[];
  sub!: Subscription;

  constructor(private boardService: BoardService) {}

  ngOnInit(): void {
    this.boardService.initializeState();
    this.sub = this.boardService.getState().subscribe((state) => {
      this.columns = state;
    });
  }

  drop(e: any): void {
    if (e.previousContainer === e.container) {
      moveItemInArray(e.container.data, e.previousIndex, e.currentIndex);
    } else {
      transferArrayItem(
        e.previousContainer.data,
        e.container.data,
        e.previousIndex,
        e.currentIndex
      );
    }
  }

  ngOnDestroy(): void{
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}

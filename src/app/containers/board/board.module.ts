import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { ColumnComponent } from './column/column.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CardComponent } from './card/card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StorageService } from 'src/app/shared/storage.service';
import { TextElementModule } from 'src/app/components/text-element/text-element.module';

@NgModule({
  declarations: [BoardComponent, ColumnComponent, CardComponent],
  imports: [
    CommonModule,
    MaterialModule,
    DragDropModule,
    ReactiveFormsModule,
    TextElementModule,
  ],
  exports: [BoardComponent],
  providers: [StorageService],
})
export class BoardModule {}

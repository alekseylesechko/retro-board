import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from 'src/app/shared/storage.service';
import { Card, Comment, LikeStatus } from './card/card.model';
import { Column } from './column/column.model';

const state = {
  columns: [new Column('Went well'), new Column('To improve', 'red')],
};

@Injectable({
  providedIn: 'root',
})
export class BoardService {
  columns: Column[] = [];
  state!: BehaviorSubject<Column[]>;

  constructor(private storageService: StorageService) {}

  initializeState(): void {
    const items = this.storageService.getItems('columns');
    if (items) {
      this.columns = items;
    } else {
      this.storageService.setItems('columns', state.columns);
      this.columns = state.columns;
    }
    this.state = new BehaviorSubject(this.columns);
  }

  getState(): Observable<Column[]> {
    return this.state.asObservable();
  }

  getColumns(): Column[] {
    return this.storageService.getItems('columns');
  }

  addColumn(column: Column): void {
    this.columns.push(column);
    this.save();
    this.updateState();
  }

  deleteColumn(columnId: string): void {
    this.columns = this.columns.filter((c) => c.id !== columnId);
    this.save();
    this.updateState();
  }

  addCard(columnId: string, card: Card): void {
    const idx = this.columns.findIndex((col) => col.id === columnId);
    this.columns[idx].items.unshift(card);
    this.save();
    this.updateState();
  }

  deleteCard(cardId: string, columnId: string): void {
    const column = this.columns.find((c) => c.id === columnId);
    if (column) {
      column.items = column.items.filter((card) => card.id !== cardId);
      this.save();
      this.updateState();
    }
  }

  addComment(comment: Comment, cardId: string, columnId: string): void {
    const card = this.foundCard(cardId, columnId);
    card?.comments.unshift(comment);
    this.save();
    this.updateState();
  }

  changeLikeStatus(
    likeStatus: LikeStatus,
    cardId: string,
    columnId: string
  ): void {
    const card = this.foundCard(cardId, columnId);
    if (card) {
      card.likeStatus =
        card.likeStatus === LikeStatus[likeStatus]
          ? LikeStatus.DEFAULT
          : LikeStatus[likeStatus];
      this.save();
      this.updateState();
    }
  }

  private updateState(): void {
    this.state.next(this.columns);
  }

  private save(): void {
    this.storageService.setItems('columns', this.columns);
  }

  private foundCard(cardId: string, columnId: string): Card | null {
    const column = this.columns.find((c) => c.id === columnId);
    const card = column?.items.find((c) => c.id === cardId);
    return card || null;
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Card, LikeStatus } from './card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() card!: Card;
  @Input() color = 'green';
  @Output() changeLike: EventEmitter<any> = new EventEmitter();
  @Output() addComment: EventEmitter<any> = new EventEmitter();
  @Output() deleteCard: EventEmitter<string> = new EventEmitter();
  commentOpen = false;

  onLikeChange(status: string): void {
    this.changeLike.emit({
      status: status as LikeStatus,
      cardId: this.card.id,
    });
  }

  onCardDelete(): void {
    this.deleteCard.emit(this.card.id);
  }

  onAddComment(comment: string): void {
    if (comment.trim()) {
      this.addComment.next({ comment, id: this.card.id });
    }
    this.commentOpen = false;
  }
}

import { generateId } from '../column/column.model';

export enum LikeStatus {
  LIKE = 'LIKE',
  DISLIKE = 'DISLIKE',
  DEFAULT = 'DEFAULT',
}

export type Comment = {
  content: string;
};

export class Card {
  constructor(
    public content: string,
    public color: string = 'green',
    public likeStatus: LikeStatus = LikeStatus.DEFAULT,
    public comments: Comment[] = [],
    public id: string = generateId()
  ) {}
}

import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BoardService } from '../board.service';
import { Card, Comment } from '../card/card.model';
import { Column } from './column.model';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss'],
})
export class ColumnComponent implements OnInit {
  @Input() column!: Column;
  form!: FormGroup;
  isAddFormOpen = false;

  constructor(public boardService: BoardService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      content: new FormControl(null, { validators: [Validators.required] }),
    });
  }

  onAddCard(): void {
    if (this.form.invalid) {
      return;
    }
    const card = new Card(this.form.value.content);
    this.boardService.addCard(this.column.id, card);
    this.form.reset();
    this.onAddToggle();
  }

  onLikeChange(event: any): void {
    this.boardService.changeLikeStatus(
      event.status,
      event.cardId,
      this.column.id
    );
  }

  onAddComment(event: any): void {
    const comment: Comment = {
      content: event.comment,
    };
    this.boardService.addComment(comment, event.id, this.column.id);
  }

  onAddToggle(): void {
    this.form.reset();
    this.isAddFormOpen = !this.isAddFormOpen;
  }

  onDeleteCard(cardId: string): void {
    this.boardService.deleteCard(cardId, this.column.id);
  }

  onColumnDelete(): void {
    this.boardService.deleteColumn(this.column.id);
  }

  onDrop(e: CdkDragDrop<{ title: string; subtitle: string }[], any>): void {
    if (e.previousContainer === e.container) {
      moveItemInArray(e.container.data, e.previousIndex, e.currentIndex);
    } else {
      transferArrayItem(
        e.previousContainer.data,
        e.container.data,
        e.previousIndex,
        e.currentIndex
      );
    }
  }
}

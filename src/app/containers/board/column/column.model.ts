import { Card } from '../card/card.model';

export class Column {
  constructor(
    public name: string,
    public color: string = 'green',
    public items: Card[] = [],
    public id: string = generateId()
  ) {}
}

export function generateId(): string {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

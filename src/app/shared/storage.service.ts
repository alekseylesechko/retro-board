import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  setItems(key: string, value: any): void {
    const val = JSON.stringify(value);
    localStorage.setItem(key, val);
  }

  getItems(key: string): any {
    const element = localStorage.getItem(key);
    return element ? JSON.parse(element) : null;
  }
}
